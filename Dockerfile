# Read more about this script on this blog post https://about.gitlab.com/2018/10/24/setting-up-gitlab-ci-for-android-projects/, by Jason Lenny
FROM openjdk:8-jdk

RUN apt-get update && apt-get install -y wget tar unzip lib32stdc++6 lib32z1

ENV ANDROID_COMPILE_SDK "28"
ENV ANDROID_BUILD_TOOLS "28.0.2"
ENV ANDROID_SDK_TOOLS "4333796"
ENV ANDROID_HOME /opt/android-sdk

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip && \
    mkdir -p "$ANDROID_HOME" && \
    unzip -d "$ANDROID_HOME" android-sdk.zip && \
    rm -f android-sdk.zip
RUN echo y | "$ANDROID_HOME"/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null && \
    echo y | "$ANDROID_HOME"/tools/bin/sdkmanager "platform-tools" >/dev/null && \
    echo y | "$ANDROID_HOME"/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null
RUN yes | "$ANDROID_HOME"/tools/bin/sdkmanager --licenses

ENV PATH $PATH:$ANDROID_HOME/platform-tools
